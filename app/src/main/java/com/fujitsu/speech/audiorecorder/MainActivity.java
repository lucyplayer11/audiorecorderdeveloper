package com.fujitsu.speech.audiorecorder;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import com.loopj.android.http.*;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    MediaRecorder myAudioRecorder;
    private String outputFile = null;
    private Button start, stop, play, translate;
    private TextView txtSrcText, txtTranslated;
    private Spinner srcLang, dstLang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        start = (Button) findViewById(R.id.button1);
        stop = (Button) findViewById(R.id.button2);
        play = (Button) findViewById(R.id.button3);
        translate = (Button) findViewById(R.id.button4);

        srcLang = (Spinner) findViewById( R.id.sourceLanguage );
        dstLang = (Spinner) findViewById( R.id.destLanguage );

        txtSrcText = (TextView) findViewById( R.id.txtSourceText );
        txtTranslated = (TextView) findViewById( R.id.txtTranslatedText );

        stop.setEnabled(false);
        play.setEnabled(false);
        //translate.setEnabled(false);

        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/audio.amr";
        initRecorder();
        initLanguages();
    }

    private void initLanguages() {
        ArrayList<Language> languageList = new ArrayList<>();

        languageList.add( new Language( "en", "English" ) );
        languageList.add( new Language( "es", "Spanish" ) );
        languageList.add( new Language( "de", "German" ) );
        languageList.add( new Language( "fr", "French" ) );
        languageList.add( new Language( "it", "Italian" ) );

        ArrayAdapter<Language> srcAdapter = new ArrayAdapter<Language>(this, android.R.layout.simple_spinner_dropdown_item, languageList );
        srcLang.setAdapter( srcAdapter );
        srcLang.setSelection( 0 );

        ArrayAdapter<Language> dstAdapter = new ArrayAdapter<Language>(this, android.R.layout.simple_spinner_dropdown_item, languageList );
        dstLang.setAdapter( dstAdapter );
        dstLang.setSelection( 1 );
    }

    private void initRecorder() {
        myAudioRecorder = new MediaRecorder();
        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
        myAudioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        myAudioRecorder.setOutputFile(outputFile);
    }

    public void start(View v) {
        try {
            myAudioRecorder.prepare();
            myAudioRecorder.start();
        } catch( IllegalStateException e ) {
            e.printStackTrace();
        } catch( IOException e ) {
            e.printStackTrace();
        } catch( Exception e ) {
            e.printStackTrace();
        }

        start.setEnabled( false );
        stop.setEnabled( true );
        play.setEnabled( false );

        toast( "Recording started" );
    }

    public void stop(View v) {
        myAudioRecorder.stop();
        myAudioRecorder.release();
        initRecorder();
        start.setEnabled( true );
        stop.setEnabled( false );
        play.setEnabled( true );
        translate.setEnabled( true );

        toast( "Audio Successfully Recorded" );
    }

    public void swap(View v) {
       int src = srcLang.getSelectedItemPosition();
       int dst = dstLang.getSelectedItemPosition();

       srcLang.setSelection( dst );
       dstLang.setSelection( src );
    }

    public void play(View v) {
        MediaPlayer m = new MediaPlayer();
        try {
            m.setDataSource(outputFile);
            m.prepare();
            m.start();
        } catch( IllegalStateException e ) {
            e.printStackTrace();
        } catch( IOException e ) {
            e.printStackTrace();
        }
        toast( "Playing Audio" );
    }

    public void translate(View v) {
        Language from = (Language)srcLang.getSelectedItem();
        Language to = (Language)dstLang.getSelectedItem();

        uploadAudio( this, from.getId(), to.getId(), outputFile );

        toast( "Translate from " + from.getName() + " to " + to.getName() );
    }

    public void uploadAudio( Context context, String sourceLang, String destLang, String filename ) {
        AsyncHttpClient client = new AsyncHttpClient();

        File file = new File( filename );
        RequestParams params = new RequestParams();
        try
        {
            params.put("audio", file );
        }
        catch( FileNotFoundException e )
        {
            e.printStackTrace();
        }

        params.put("srclang", sourceLang );
        params.put("dstlang", destLang );
        client.post(context, "http://192.168.1.18:8880/translations/upload.json", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                toast( "Starting" );
            }

            @Override
            public void onFailure( int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                toast("Failed..");
            }

            @Override
            public void onSuccess( int statusCode, Header[] headers, byte[] responseBody ) {
                toast("Success..");

                try {
                    JSONObject jsonObj = new JSONObject( new String( responseBody ) );
                    String srcText = jsonObj.getString( "Text" );
                    String translation = jsonObj.getString( "Translation" );

                    txtSrcText.setText( Html.fromHtml( srcText ) );
                    txtTranslated.setText( Html.fromHtml( translation ) );
                } catch ( JSONException e ) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRetry( int retryNo ) {
                toast( "Retrying ... " + retryNo );
            }

        });

    }

    public void toast( String message ) {
        Toast.makeText( this, message, Toast.LENGTH_SHORT ).show();
    }
}
