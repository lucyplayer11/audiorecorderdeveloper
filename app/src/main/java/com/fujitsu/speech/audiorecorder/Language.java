package com.fujitsu.speech.audiorecorder;

/**
 * Created by BromleyS on 23/11/2017.
 */

public class Language {
    private String id;
    private String name;

    public Language( String id, String name )
    {
        this.id = id;
        this.name = name;
    }
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Language){
            Language l = (Language)obj;
            if(l.getName().equals(name) && l.getId().equals(id) ) return true;
        }

        return false;
    }
}
